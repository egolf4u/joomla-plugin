<?php
/**
 * @version    1.0
 * @package    EGolf4U.System
 * @subpackage Plugins
 * @license    GNU/GPL
 */
 
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

JHtml::_('jquery.framework',  true, true);

/**
 * E-Golf4U Authenticate Plugin
 *
 * A plugin to authenticate against the E-Golf4U database
 *
 * @package    EGolf4U.System
 * @subpackage Plugins
 * @license    GNU/GPL
 */
class PlgSystemEGolf4U extends JPlugin
{
    public function _build_egolf4u_iframe($environment){
        return '<iframe src="http://' .$environment . '.e-golf4u.nl/leden/iframe" style="width: 100%; border: 0;" allowtransparancy="true" frameborder="0"></iframe>';
    }

    public function _build_login_redirect($environment, $username, $password){
        return '
            <form method="post" action="https://api.e-golf4u.nl/auth/login" id="egolf4u_login_form">
                <input type="hidden" name="environment" value="' . $environment .  '" />
                <input type="hidden" name="username" value="' . $username .  '" />
                <input type="hidden" name="password" value="' . $password .  '" />
                <input type="hidden" name="return_url" value="http://websites.egolf4u.lcl/joomla/index.php?option=com_content&view=article&id=2" />
            </form>
            <script>document.getElementById("egolf4u_login_form").submit();</script>
        ';
    }

    public function onBeforeRender()
    {
        $environment = $this->params->get('environment');   
        
        if(isset($_SESSION["EGOLF4U_LOGIN_STATE"]) && $_SESSION["EGOLF4U_LOGIN_STATE"] == "logging_in"){
            $_SESSION["EGOLF4U_LOGIN_STATE"] = "logged_in";

            echo $this->_build_login_redirect($environment, $_SESSION["EGOLF4U_USERNAME"], $_SESSION["EGOLF4U_PASSWORD"]); 
        }
    
        return true;
    }

    public function onAfterRender()
    {
        $logout_binding = "
            <script>
            jQuery(\"input[value='Log out']\").on('click', function(){
                var logout_button = jQuery(this);
                logout_button.val('Uitloggen....');
                var wndw = window.open('https://api.e-golf4u.nl/auth/logout','windowName','height=10,width=10');
                wndw.blur();
                window.focus();
                setTimeout(function(){
                    logout_button.parents('form').submit();
                    wndw.close();
                }, 1000);
                return false;
            });
            </script>
        ";

        $body = JResponse::getBody();
        $body = str_replace("</body>", $logout_binding . "</body>", $body);

        JResponse::setBody($body);
    }
}
