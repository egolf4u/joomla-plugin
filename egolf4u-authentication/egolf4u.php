<?php
/**
 * @version    1.0
 * @package    EGolf4U.Authenticate
 * @subpackage Plugins
 * @license    GNU/GPL
 */
 
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport( 'joomla.plugin.plugin' );

/**
 * E-Golf4U Authenticate Plugin
 *
 * A plugin to authenticate against the E-Golf4U database
 *
 * @package    EGolf4U.Authenticate
 * @subpackage Plugins
 * @license    GNU/GPL
 */
class plgAuthenticationEGolf4U extends JPlugin
{
    

    public function __construct(& $subject, $config)
    {
        parent::__construct($subject, $config);

        $this->loadLanguage('', JPATH_ADMINISTRATOR);
    }

    /**
     *
     * @access    public
     * @param     array     $credentials    Array holding the user credentials ('username' and 'password')
     * @param     array     $options        Array of extra options
     * @param     object    $response       Authentication response object
     * @return    boolean
     */
    public function onUserAuthenticate( $credentials, $options, &$response )
    {
        jimport('joomla.user.helper');

        $environment = $this->params->get('environment');

        $apiurl = "https://api.e-golf4u.nl/auth/validate";


        if(empty($environment)) {
            JError::raiseWarning(500, "E-Golf4U Auth :: Environment not set");
            return;
        }


        // Empty username/password can be interpreted as anonymous auth.
        if (empty($credentials['username']) || empty($credentials['password'])) {
            $response->status = JAuthentication::STATUS_FAILURE;
            $response->error_message =
                JText::_(
                    'JGLOBAL_AUTH_FAILED',
                    JText::_('JGLOBAL_AUTH_EMPTY_PASS_NOT_ALLOWED')
                );
            return;
        }

        //Build validate request
        $apiurl .= "?environment=" . urlencode($environment);
        $apiurl .= "&username=" . urlencode($credentials['username']);
        $apiurl .= "&password=" . urlencode($credentials['password']);

        $req = curl_init($apiurl);
        //curl_setopt($req, CURLOPT_USERPWD, "zaanse" . ":" . "Qopjds2309jsn2807829hBN2n2e89");
        curl_setopt($req, CURLOPT_TIMEOUT, 30);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, TRUE);
        $return = curl_exec($req);

        $_SESSION["EGOLF4U_LOGIN_STATE"] = null;
        $_SESSION["EGOLF4U_USERNAME"] = null;
        $_SESSION["EGOLF4U_PASSWORD"] = null;

        if($response = json_decode($return)){
            if($response->valid){
                $response = new stdClass();

                $response->email = $credentials['username'] . "@" . $environment . ".e-golf4u.nl";
                $response->username = $credentials['username'];
                $response->password = $credentials['password'];
                $response->status = JAuthentication::STATUS_SUCCESS;
                $response->error_message = '';

                $_SESSION["EGOLF4U_LOGIN_STATE"] = "logging_in";
                $_SESSION["EGOLF4U_USERNAME"] = $credentials['username'];
                $_SESSION["EGOLF4U_PASSWORD"] = $credentials['password'];
            }else{
                $response->status = JAuthentication::STATUS_FAILURE;
                $response->error_message = 'Ongeldige gebruikersnaam en/of wachtwoord';
            }
        }
    }


}
?>